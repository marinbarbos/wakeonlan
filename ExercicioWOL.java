import java.io.*;
import java.util.Scanner;
import java.net.*;

public class ExercicioWOL {
    public static void main(String[] args) {
        String mac = args[0];
        String ip = args[1];
        int porta = Integer.parseInt(args[2]);

        try {
            byte[] bytesMac = new byte[6];
            String[] splitMac = mac.split("(\\:|\\-)");
            for (int i = 0; i < 6; i++) {
			            bytesMac[i] = (byte) Integer.parseInt(splitMac[i], 16);
            }

            byte[] bytes = new byte[6 + (16 * byteMac.length)];
            for (int i = 0; i < 6; i++) {
                bytes[i] = (byte) 0xff;
            }
            for (int i = 6; i < bytes.length; i += bytesMac.length) {
                System.arraycopy(bytesMac, 0, bytes, i, bytesMac.length);
            }

            InetAddress ipBroadcast = InetAddress.getByName(ip);
            DatagramPacket pacote = new DatagramPacket(bytes, bytes.length, ipBroadcast, porta);
            DatagramSocket socket = new DatagramSocket();
            socket.send(pacote);
            socket.close();

            System.out.println("=== Wake-on-LAN enviado ===");
        }
        catch (Exception e) {
            System.out.println("=== Wake-on-LAN nao enviado: ===\n"+ e);
        }

    }
}